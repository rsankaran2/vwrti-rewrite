package com.vims.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ui_field_type")
public class UIFieldType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1715197461540286823L;

	@Id
	@Column(name="ui_field_type_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int UiFieldTypeId;
	
	@Column(name="ui_field_type_name",nullable=false, unique=true)
	private String UiFieldTypeName;

	public int getUiFieldTypeId() {
		return UiFieldTypeId;
	}

	public void setUiFieldTypeId(int uiFieldTypeId) {
		UiFieldTypeId = uiFieldTypeId;
	}

	public String getUiFieldTypeName() {
		return UiFieldTypeName;
	}

	public void setUiFieldTypeName(String uiFieldTypeName) {
		UiFieldTypeName = uiFieldTypeName;
	}
	
}
