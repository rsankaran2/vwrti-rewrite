package com.vims.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="type_category")
public class TypeCategory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7351142278503364720L;

	@Id
	@Column(name="type_category_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int typeCategoryId;

	@Column(name="type_category_name",nullable=false)
	private String typeCategoryName;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="category")
	private List<TypeSubCategory> subCategories;
	
	
	public TypeCategory() {
    }

	/**
	 * @return the typeCategoryId
	 */
	public int getTypeCategoryId() {
		return typeCategoryId;
	}

	/**
	 * @param typeCategoryId the typeCategoryId to set
	 */
	public void setTypeCategoryId(int typeCategoryId) {
		this.typeCategoryId = typeCategoryId;
	}

	/**
	 * @return the typeCategoryName
	 */
	public String getTypeCategoryName() {
		return typeCategoryName;
	}

	/**
	 * @param typeCategoryName the typeCategoryName to set
	 */
	public void setTypeCategoryName(String typeCategoryName) {
		this.typeCategoryName = typeCategoryName;
	}

	/**
	 * @return the subCategories
	 */
	public List<TypeSubCategory> getSubCategories() {
		return subCategories;
	}

	/**
	 * @param subCategories the subCategories to set
	 */
	public void setSubCategories(List<TypeSubCategory> subCategories) {
		this.subCategories = subCategories;
	}

	
}
