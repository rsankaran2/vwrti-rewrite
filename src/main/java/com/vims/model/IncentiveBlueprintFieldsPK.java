package com.vims.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class IncentiveBlueprintFieldsPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -977582120001274006L;
	
	@ManyToOne
	@JoinColumn(name="incentive_blueprint_id")
	private IncentiveBlueprint incentiveBlueprint;
	
	@ManyToOne
	@JoinColumn(name="type_field_id")
	private TypeField typeField;

	
	public IncentiveBlueprint getIncentiveBlueprint() {
		return incentiveBlueprint;
	}

	public void setIncentiveBlueprint(IncentiveBlueprint incentiveBlueprint) {
		this.incentiveBlueprint = incentiveBlueprint;
	}

	public TypeField getTypeField() {
		return typeField;
	}

	public void setTypeField(TypeField typeField) {
		this.typeField = typeField;
	}

}
