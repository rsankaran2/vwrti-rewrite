package com.vims.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="priveleges")
public class Priveleges implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7351142278503364720L;

	@Id
	@Column(name="priv_id",unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int privId;

	@Column(name="priv_name",nullable=false,unique = true)
	private String privName;
	
	@Column(name="priv_code",nullable=false,unique = true)
	private String privCode;
	
	//@ManyToMany(mappedBy = "priveleges")
	//private Set<Roles> roles;
	
	public Priveleges(){
		
	}

	
	/**
	 * @return the privId
	 */
	public int getPrivId() {
		return privId;
	}


	/**
	 * @param privId the privId to set
	 */
	public void setPrivId(int privId) {
		this.privId = privId;
	}

	/**
	 * @return the privName
	 */
	public String getPrivName() {
		return privName;
	}

	/**
	 * @param privName the privName to set
	 */
	public void setPrivName(String privName) {
		this.privName = privName;
	}

	/**
	 * @return the privCode
	 */
	public String getPrivCode() {
		return privCode;
	}

	/**
	 * @param privCode the privCode to set
	 */
	public void setPrivCode(String privCode) {
		this.privCode = privCode;
	}

}
