package com.vims.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="vehicle_model")
public class VehicleModel implements Serializable {



	/**
	 * 
	 */
	private static final long serialVersionUID = -225999988171040458L;

	@Id
	@Column(name="vehicle_model_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int vehicleModelId;
	
	@Column(name="vehicle_model_code",nullable=false)
	private String vehicleModelCode;
	
	@Column(name="vehicle_model_year",nullable=false, unique=true )
	private String vehicleModelYear;
	
	@Column(name="vehicle_model_name",nullable=false)
	private String vehicleModelName;
	
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "vehicleModelFieldsPK.vehicleModel")
    protected List<VehicleModelFields> modelFields;
	

	public List<VehicleModelFields> getModelFields() {
		return modelFields;
	}

	public void setModelFields(List<VehicleModelFields> modelFields) {
		this.modelFields = modelFields;
	}

	public int getVehicleModelId() {
		return vehicleModelId;
	}

	public void setVehicleModelId(int vehicleModelId) {
		this.vehicleModelId = vehicleModelId;
	}

	public String getVehicleModelCode() {
		return vehicleModelCode;
	}

	public void setVehicleModelCode(String vehicleModelCode) {
		this.vehicleModelCode = vehicleModelCode;
	}

	public String getVehicleModelYear() {
		return vehicleModelYear;
	}

	public void setVehicleModelYear(String vehicleModelYear) {
		this.vehicleModelYear = vehicleModelYear;
	}

	public String getVehicleModelName() {
		return vehicleModelName;
	}

	public void setVehicleModelName(String vehicleModelName) {
		this.vehicleModelName = vehicleModelName;
	}
	
}
