package com.vims.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="vehicle_model_fields")
public class VehicleModelFields implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6222274020367109040L;
	
	@EmbeddedId
	private VehicleModelFieldsPK vehicleModelFieldsPK;
	
	@Column(name="type_field_value",nullable=false)
	private String typeFieldValue;
	
	
	public VehicleModelFieldsPK getVehicleModelFieldsPK() {
		return vehicleModelFieldsPK;
	}


	public void setVehicleModelFieldsPK(VehicleModelFieldsPK vehicleModelFieldsPK) {
		this.vehicleModelFieldsPK = vehicleModelFieldsPK;
	}


	public String getTypeFieldValue() {
		return typeFieldValue;
	}


	public void setTypeFieldValue(String typeFieldValue) {
		this.typeFieldValue = typeFieldValue;
	}

	
	

}
