package com.vims.model;


import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="incentive_blueprint")
public class IncentiveBlueprint implements Serializable
{
	 private final static long serialVersionUID = 6203490157687054993L;
	@Id
	@Column(name="incentive_blueprint_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int incentiveBlueprintId;

    /**
     * The incentive blueprint name
     * 
     */
    @Column(name="incentive_blueprint_name",nullable=false)
    private String incentiveBlueprintName;
    /**
     * The status
     * 
     */
    @Column(name="status",nullable=false)
    private String status;
    /**
     * user id of the creator
     * 
     */
    @Column(name="created_by_user_id",nullable=false)
    private String createdByUserId;
    /**
     * created date
     * 
     */
    @Column(name="created_date",nullable=false)
    private String createdDate;
    /**
     * user id of the approver
     * 
     */
    @Column(name="approved_by_user_id")
    private String approvedByUserId;
    /**
     * approved date
     * 
     */
    @Column(name="approved_date")
    private String approvedDate;
    /**
     * comments
     * 
     */
    @Column(name="comments")
    private String comments;
    /**
     * List of fields under incentive blueprint
     * 
     */
    @OneToMany(cascade=CascadeType.ALL, mappedBy = "incentiveBlueprintFieldsPK.incentiveBlueprint")
    protected List<IncentiveBlueprintFields> modelFields;
   

    public int getIncentiveBlueprintId() {
		return incentiveBlueprintId;
	}

	public void setIncentiveBlueprintId(int incentiveBlueprintId) {
		this.incentiveBlueprintId = incentiveBlueprintId;
	}

	public List<IncentiveBlueprintFields> getModelFields() {
		return modelFields;
	}

	public void setModelFields(List<IncentiveBlueprintFields> modelFields) {
		this.modelFields = modelFields;
	}

	/**
     * No args constructor for use in serialization
     * 
     */
    public IncentiveBlueprint() {
    }

    /**
     * 
     * @param incentiveBlueprintFields
     * @param createdByUserId
     * @param status
     * @param approvedDate
     * @param incentiveBlueprintName
     * @param createdDate
     * @param comments
     * @param approvedByUserId
     */
    public IncentiveBlueprint(String incentiveBlueprintName, String status, String createdByUserId, String createdDate, String approvedByUserId, String approvedDate, String comments, List<IncentiveBlueprintFields> incentiveBlueprintFields) {
        super();
        this.incentiveBlueprintName = incentiveBlueprintName;
        this.status = status;
        this.createdByUserId = createdByUserId;
        this.createdDate = createdDate;
        this.approvedByUserId = approvedByUserId;
        this.approvedDate = approvedDate;
        this.comments = comments;
        this.modelFields = incentiveBlueprintFields;
    }

    /**
     * The incentive blueprint name
     * 
     */
    public String getIncentiveBlueprintName() {
        return incentiveBlueprintName;
    }

    /**
     * The incentive blueprint name
     * 
     */
    public void setIncentiveBlueprintName(String incentiveBlueprintName) {
        this.incentiveBlueprintName = incentiveBlueprintName;
    }

    /**
     * The status
     * 
     */
    public String getStatus() {
        return status;
    }

    /**
     * The status
     * 
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * user id of the creator
     * 
     */
    public String getCreatedByUserId() {
        return createdByUserId;
    }

    /**
     * user id of the creator
     * 
     */
    public void setCreatedByUserId(String createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    /**
     * created date
     * 
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * created date
     * 
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * user id of the approver
     * 
     */
    public String getApprovedByUserId() {
        return approvedByUserId;
    }

    /**
     * user id of the approver
     * 
     */
    public void setApprovedByUserId(String approvedByUserId) {
        this.approvedByUserId = approvedByUserId;
    }

    /**
     * approved date
     * 
     */
    public String getApprovedDate() {
        return approvedDate;
    }

    /**
     * approved date
     * 
     */
    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    /**
     * comments
     * 
     */
    public String getComments() {
        return comments;
    }

    /**
     * comments
     * 
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

   


}
