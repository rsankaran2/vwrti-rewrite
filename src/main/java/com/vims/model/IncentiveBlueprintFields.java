package com.vims.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="incentive_blueprint_fields")
public class IncentiveBlueprintFields implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4200046908108142481L;

	@EmbeddedId
	private IncentiveBlueprintFieldsPK incentiveBlueprintFieldsPK;
	
	@Column(name="type_field_value",nullable=false)
	private String typeFieldValue;
	
	
	


	public IncentiveBlueprintFieldsPK getIncentiveBlueprintFieldsPK() {
		return incentiveBlueprintFieldsPK;
	}


	public void setIncentiveBlueprintFieldsPK(
			IncentiveBlueprintFieldsPK incentiveBlueprintFieldsPK) {
		this.incentiveBlueprintFieldsPK = incentiveBlueprintFieldsPK;
	}


	public String getTypeFieldValue() {
		return typeFieldValue;
	}


	public void setTypeFieldValue(String typeFieldValue) {
		this.typeFieldValue = typeFieldValue;
	}

}
