package com.vims.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="type_field")
public class TypeField implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1564279596417505917L;

	@Id
	@Column(name="type_field_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int typeFieldId;

	@Column(name="type_field_name",nullable=false,unique=true)
	private String typeFieldName;
	
	@Column(name="type_field_default_value")
	private String typeFieldDefaultValue;
	
	@Column(name="type_field_data_type",nullable=false)
	private String typeFieldDataType;
	
	@ManyToOne
	@JoinColumn(name="type_sub_category_id")
	private TypeSubCategory subCategory;
	
	@ManyToOne
	@JoinColumn(name="ui_field_type_id")
	private UIFieldType uiFieldType;
	
	@Column(name="type_field_label_type")
	private String typeFieldLabelType;
	
	
	public TypeField(){
		
	}
	
	public String getTypeFieldLabelType() {
		return typeFieldLabelType;
	}


	public void setTypeFieldLabelType(String typeFieldLabelType) {
		this.typeFieldLabelType = typeFieldLabelType;
	}


	


	/**
	 * @return the typeFieldId
	 */
	public int getTypeFieldId() {
		return typeFieldId;
	}


	/**
	 * @param typeFieldId the typeFieldId to set
	 */
	public void setTypeFieldId(int typeFieldId) {
		this.typeFieldId = typeFieldId;
	}


	/**
	 * @return the typeFieldName
	 */
	public String getTypeFieldName() {
		return typeFieldName;
	}


	/**
	 * @param typeFieldName the typeFieldName to set
	 */
	public void setTypeFieldName(String typeFieldName) {
		this.typeFieldName = typeFieldName;
	}


	/**
	 * @return the typeFieldDefaultValue
	 */
	public String getTypeFieldDefaultValue() {
		return typeFieldDefaultValue;
	}


	/**
	 * @param typeFieldDefaultValue the typeFieldDefaultValue to set
	 */
	public void setTypeFieldDefaultValue(String typeFieldDefaultValue) {
		this.typeFieldDefaultValue = typeFieldDefaultValue;
	}


	/**
	 * @return the typeFieldDataType
	 */
	public String getTypeFieldDataType() {
		return typeFieldDataType;
	}


	/**
	 * @param typeFieldDataType the typeFieldDataType to set
	 */
	public void setTypeFieldDataType(String typeFieldDataType) {
		this.typeFieldDataType = typeFieldDataType;
	}


	/**
	 * @return the subCategory
	 */
	public TypeSubCategory getSubCategory() {
		return subCategory;
	}


	/**
	 * @param subCategory the subCategory to set
	 */
	public void setSubCategory(TypeSubCategory subCategory) {
		this.subCategory = subCategory;
	}


	public UIFieldType getUiFieldType() {
		return uiFieldType;
	}


	public void setUiFieldType(UIFieldType uiFieldType) {
		this.uiFieldType = uiFieldType;
	}

	
}
