package com.vims.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
@Table(name="roles")
public class Roles implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7351142278503364720L;

	@Id
	@Column(name="role_id",unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int roleId;

	@Column(name="role_code",nullable=false,unique = true)
	private String roleCode;
	
	@Column(name="role_name",nullable=false,unique = true)
	private String roleName;
	
	/*@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "roles_priveleges",joinColumns = {
			@JoinColumn(name = "role_id", nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name = "priv_id",
					nullable = false, updatable = false) })*/
	@ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinTable(name = "roles_priveleges")
	@Fetch(FetchMode.SUBSELECT)
	private Set<Priveleges> priveleges;
	
	
	public Roles(){
		
	}

	/**
	 * @return the roleId
	 */
	public int getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the roleCode
	 */
	public String getRoleCode() {
		return roleCode;
	}

	/**
	 * @param roleCode the roleCode to set
	 */
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * @param roleName the roleName to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the priveleges
	 */
	public Set<Priveleges> getPriveleges() {
		return priveleges;
	}

	/**
	 * @param priveleges the priveleges to set
	 */
	public void setPriveleges(Set<Priveleges> priveleges) {
		this.priveleges = priveleges;
	}

}
