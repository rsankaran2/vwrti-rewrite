package com.vims.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="type_sub_category")
public class TypeSubCategory implements Serializable{



	/**
	 * 
	 */
	private static final long serialVersionUID = -2087255591760558887L;

	@Id
	@Column(name="type_sub_category_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int typeSubCategoryId;

	@Column(name="type_sub_category_name",nullable=false)
	private String typeSubCategoryName;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="subCategory")
	private List<TypeField> typeFields;
	
	@ManyToOne
	@JoinColumn(name="type_category_id")
	private TypeCategory category;
	
	
	public TypeSubCategory(){
		
	}

	/**
	 * @return the typeSubCategoryId
	 */
	public int getTypeSubCategoryId() {
		return typeSubCategoryId;
	}

	/**
	 * @param typeSubCategoryId the typeSubCategoryId to set
	 */
	public void setTypeSubCategoryId(int typeSubCategoryId) {
		this.typeSubCategoryId = typeSubCategoryId;
	}

	/**
	 * @return the typeSubCategoryName
	 */
	public String getTypeSubCategoryName() {
		return typeSubCategoryName;
	}

	/**
	 * @param typeSubCategoryName the typeSubCategoryName to set
	 */
	public void setTypeSubCategoryName(String typeSubCategoryName) {
		this.typeSubCategoryName = typeSubCategoryName;
	}

	/**
	 * @return the typeFields
	 */
	public List<TypeField> getTypeFields() {
		return typeFields;
	}

	/**
	 * @param typeFields the typeFields to set
	 */
	public void setTypeFields(List<TypeField> typeFields) {
		this.typeFields = typeFields;
	}

	/**
	 * @return the category
	 */
	public TypeCategory getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(TypeCategory category) {
		this.category = category;
	}

}
