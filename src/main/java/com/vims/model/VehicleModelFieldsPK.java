package com.vims.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class VehicleModelFieldsPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -977582120001274006L;
	
	@ManyToOne
	//@Cascade(value={org.hibernate.annotations.CascadeType.ALL})
	@JoinColumn(name="vehicle_model_id")
	private VehicleModel vehicleModel;
	
	@ManyToOne
	//@Cascade(value={org.hibernate.annotations.CascadeType.ALL})
	@JoinColumn(name="type_field_id")
	private TypeField typeField;

	public VehicleModel getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(VehicleModel vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public TypeField getTypeField() {
		return typeField;
	}

	public void setTypeField(TypeField typeField) {
		this.typeField = typeField;
	}

}
