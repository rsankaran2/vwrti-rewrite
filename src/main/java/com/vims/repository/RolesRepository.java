/**
 * 
 */
package com.vims.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vims.model.Roles;



@Repository
public interface RolesRepository extends JpaRepository<Roles, Long>{
	
	@Query("SELECT r FROM Roles r ")
	public List<Roles> findAllRoles();
	
	@Query("SELECT r FROM Roles r WHERE r.roleCode = :roleCode")
	public Roles findRoleByCode(@Param("roleCode") String roleCode);
	
}
