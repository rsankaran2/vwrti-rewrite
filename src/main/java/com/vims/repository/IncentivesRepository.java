/**
 * 
 */
package com.vims.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vims.model.IncentiveBlueprint;



@Repository
public interface IncentivesRepository extends JpaRepository<IncentiveBlueprint, Long>{
	
	@Query("SELECT c FROM IncentiveBlueprint c WHERE LOWER(c.incentiveBlueprintName) = LOWER(:name)")
	public IncentiveBlueprint findByName(String name);



}
