/**
 * 
 */
package com.vims.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vims.model.TypeSubCategory;

/**
 * @author NGoyal
 *
 */
@Repository
public interface TypeSubCategoryRepository extends JpaRepository<TypeSubCategory, Long>{
	
	@Query("SELECT c FROM TypeSubCategory c WHERE LOWER(c.typeSubCategoryName) = LOWER(:typeSubCategoryName)")
	public TypeSubCategory findByName(@Param("typeSubCategoryName") String typeSubCategoryName);

}
