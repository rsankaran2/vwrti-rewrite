/**
 * 
 */
package com.vims.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vims.model.TypeCategory;

/**
 * @author NGoyal
 *
 */
@Repository
public interface TypeCategoryRepository extends JpaRepository<TypeCategory, Long>{
	
	@Query("SELECT c FROM TypeCategory c WHERE LOWER(c.typeCategoryName) = LOWER(:typeCategoryName)")
	public TypeCategory findByName(@Param("typeCategoryName") String typeCategoryName);
	
	

}
