/**
 * 
 */
package com.vims.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vims.model.VehicleModel;

/**
 * @author NGoyal
 *
 */
@Repository
public interface VehicleRepository extends JpaRepository<VehicleModel, Long>{
	
	@Query("SELECT c FROM VehicleModel c WHERE LOWER(c.vehicleModelName) = LOWER(:name)")
	public VehicleModel findByName(String name);

}
