/**
 * 
 */
package com.vims.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vims.model.Priveleges;



@Repository
public interface PrivelegeRepository extends JpaRepository<Priveleges, Long>{
	
	/*@Query("SELECT p FROM Priveleges p ")
	public List<Priveleges> findAllPriveleges();*/
	
	/*@Query("SELECT p FROM Priveleges p INNER JOIN p.roles r WHERE r.roleCode = :roleCode")
	public List<Priveleges> findPrivelegesForRole(@Param("roleCode") String roleCode);*/



}
