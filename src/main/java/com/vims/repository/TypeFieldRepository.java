/**
 * 
 */
package com.vims.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vims.model.TypeField;

/**
 * @author NGoyal
 *
 */
@Repository
public interface TypeFieldRepository extends JpaRepository<TypeField, Long>{
	
	@Query("SELECT c FROM TypeField c WHERE LOWER(c.typeFieldName) = LOWER(:name)")
	public TypeField findTypeFieldByName(@Param("name") String name);
	
	@Query("SELECT t FROM TypeField t where t.subCategory.typeSubCategoryId = :id") 
    public List<TypeField> findTypeFieldById(@Param("id") Integer id);

}
