/**
 * 
 */
package com.vims.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author NGoyal
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "categoryName", "subCategoryList" })
public class Category {

	/**
	 * The name of the category
	 * 
	 */
	@JsonProperty("categoryName")
	@JsonPropertyDescription("The name of the category")
	private String categoryName;
	
	
	/**
	 * List of Sub Categories
	 * 
	 */
	@JsonProperty("subCategoryList")
	@JsonPropertyDescription("List of Sub Categories")
	private List<SubCategory> subCategoryList = null;
	
	/**
	 * The name of the category
	 * 
	 */
	@JsonProperty("categoryName")
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * The name of the category
	 * 
	 */
	@JsonProperty("categoryName")
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * List of Sub Categories
	 * 
	 */
	@JsonProperty("subCategoryList")
	public List<SubCategory> getSubCategoryList() {
		return subCategoryList;
	}

	/**
	 * List of Sub Categories
	 * 
	 */
	@JsonProperty("subCategoryList")
	public void setSubCategoryList(List<SubCategory> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}

}
