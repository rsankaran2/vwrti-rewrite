/**
 * 
 */
package com.vims.dto;

import java.util.List;

/**
 * @author NGoyal
 *
 */
public class SubCategoryWrapper {

	private List<SubCategory> subCategoryList;

	/**
	 * @return the subCategoryList
	 */
	public List<SubCategory> getSubCategoryList() {
		return subCategoryList;
	}

	/**
	 * @param subCategoryList the subCategoryList to set
	 */
	public void setSubCategoryList(List<SubCategory> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}

	
	
	
}
