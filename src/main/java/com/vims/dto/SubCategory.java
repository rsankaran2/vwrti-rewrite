/**
 * 
 */
package com.vims.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author NGoyal
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "subCategoryName", "typeFields" })
public class SubCategory{

	/**
	 * The name of the sub category
	 * 
	 */
	@JsonProperty("subCategoryName")
	@JsonPropertyDescription("The name of the sub category")
	private String subCategoryName;
	/**
	 * List of fields under a sub category
	 * 
	 */
	@JsonProperty("typeFields")
	@JsonPropertyDescription("List of fields under a sub category")
	private List<Field> typeFields = null;

	/**
	 * The name of the sub category
	 * 
	 */
	@JsonProperty("subCategoryName")
	public String getSubCategoryName() {
		return subCategoryName;
	}

	/**
	 * The name of the sub category
	 * 
	 */
	@JsonProperty("subCategoryName")
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	/**
	 * List of fields under a sub category
	 * 
	 */
	@JsonProperty("typeFields")
	public List<Field> getTypeFields() {
		return typeFields;
	}

	/**
	 * List of fields under a sub category
	 * 
	 */
	@JsonProperty("typeFields")
	public void setTypeFields(List<Field> typeFields) {
		this.typeFields = typeFields;
	}

}
