
package com.vims.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "fieldName",
    "fieldValue"
})
public class FieldDto implements Serializable
{

    /**
     * The name of the field 
     * 
     */
    @JsonProperty("fieldName")
    @JsonPropertyDescription("The name of the field ")
    private String fieldName;
    /**
     * The value set for the field
     * 
     */
    @JsonProperty("fieldValue")
    @JsonPropertyDescription("The value set for the field")
    private String fieldValue;
    private final static long serialVersionUID = -1955606351475719401L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public FieldDto() {
    }

    /**
     * 
     * @param fieldValue
     * @param fieldName
     */
    public FieldDto(String fieldName, String fieldValue) {
        super();
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    /**
     * The name of the field 
     * 
     */
    @JsonProperty("fieldName")
    public String getFieldName() {
        return fieldName;
    }

    /**
     * The name of the field 
     * 
     */
    @JsonProperty("fieldName")
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * The value set for the field
     * 
     */
    @JsonProperty("fieldValue")
    public String getFieldValue() {
        return fieldValue;
    }

    /**
     * The value set for the field
     * 
     */
    @JsonProperty("fieldValue")
    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(fieldName).append(fieldValue).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof FieldDto) == false) {
            return false;
        }
        FieldDto rhs = ((FieldDto) other);
        return new EqualsBuilder().append(fieldName, rhs.fieldName).append(fieldValue, rhs.fieldValue).isEquals();
    }

}
