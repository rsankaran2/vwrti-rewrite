
package com.vims.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "incentiveBlueprintName",
    "status",
    "createdByUserId",
    "createdDate",
    "ApprovedByUserId",
    "ApprovedDate",
    "comments",
    "incentiveBlueprintFields"
})
public class IncentiveBlueprintDto implements Serializable
{

    /**
     * The incentive blueprint name
     * 
     */
    @JsonProperty("incentiveBlueprintName")
    @JsonPropertyDescription("The incentive blueprint name")
    private String incentiveBlueprintName;
    /**
     * The status
     * 
     */
    @JsonProperty("status")
    @JsonPropertyDescription("The status")
    private String status;
    /**
     * user id of the creator
     * 
     */
    @JsonProperty("createdByUserId")
    @JsonPropertyDescription("user id of the creator")
    private String createdByUserId;
    /**
     * created date
     * 
     */
    @JsonProperty("createdDate")
    @JsonPropertyDescription("created date")
    private String createdDate;
    /**
     * user id of the approver
     * 
     */
    @JsonProperty("ApprovedByUserId")
    @JsonPropertyDescription("user id of the approver")
    private String approvedByUserId;
    /**
     * approved date
     * 
     */
    @JsonProperty("ApprovedDate")
    @JsonPropertyDescription("approved date")
    private String approvedDate;
    /**
     * comments
     * 
     */
    @JsonProperty("comments")
    @JsonPropertyDescription("comments")
    private String comments;
    /**
     * List of fields under incentive blueprint
     * 
     */
    @JsonProperty("incentiveBlueprintFields")
    @JsonPropertyDescription("List of fields under incentive blueprint")
    @Valid
    private List<FieldDto> incentiveBlueprintFields = new ArrayList<FieldDto>();
    private final static long serialVersionUID = 6203490157687054993L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public IncentiveBlueprintDto() {
    }

    /**
     * 
     * @param incentiveBlueprintFields
     * @param createdByUserId
     * @param status
     * @param approvedDate
     * @param incentiveBlueprintName
     * @param createdDate
     * @param comments
     * @param approvedByUserId
     */
    public IncentiveBlueprintDto(String incentiveBlueprintName, String status, String createdByUserId, String createdDate, String approvedByUserId, String approvedDate, String comments, List<FieldDto> incentiveBlueprintFields) {
        super();
        this.incentiveBlueprintName = incentiveBlueprintName;
        this.status = status;
        this.createdByUserId = createdByUserId;
        this.createdDate = createdDate;
        this.approvedByUserId = approvedByUserId;
        this.approvedDate = approvedDate;
        this.comments = comments;
        this.incentiveBlueprintFields = incentiveBlueprintFields;
    }

    /**
     * The incentive blueprint name
     * 
     */
    @JsonProperty("incentiveBlueprintName")
    public String getIncentiveBlueprintName() {
        return incentiveBlueprintName;
    }

    /**
     * The incentive blueprint name
     * 
     */
    @JsonProperty("incentiveBlueprintName")
    public void setIncentiveBlueprintName(String incentiveBlueprintName) {
        this.incentiveBlueprintName = incentiveBlueprintName;
    }

    /**
     * The status
     * 
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * The status
     * 
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * user id of the creator
     * 
     */
    @JsonProperty("createdByUserId")
    public String getCreatedByUserId() {
        return createdByUserId;
    }

    /**
     * user id of the creator
     * 
     */
    @JsonProperty("createdByUserId")
    public void setCreatedByUserId(String createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    /**
     * created date
     * 
     */
    @JsonProperty("createdDate")
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * created date
     * 
     */
    @JsonProperty("createdDate")
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * user id of the approver
     * 
     */
    @JsonProperty("ApprovedByUserId")
    public String getApprovedByUserId() {
        return approvedByUserId;
    }

    /**
     * user id of the approver
     * 
     */
    @JsonProperty("ApprovedByUserId")
    public void setApprovedByUserId(String approvedByUserId) {
        this.approvedByUserId = approvedByUserId;
    }

    /**
     * approved date
     * 
     */
    @JsonProperty("ApprovedDate")
    public String getApprovedDate() {
        return approvedDate;
    }

    /**
     * approved date
     * 
     */
    @JsonProperty("ApprovedDate")
    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    /**
     * comments
     * 
     */
    @JsonProperty("comments")
    public String getComments() {
        return comments;
    }

    /**
     * comments
     * 
     */
    @JsonProperty("comments")
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * List of fields under incentive blueprint
     * 
     */
    @JsonProperty("incentiveBlueprintFields")
    public List<FieldDto> getIncentiveBlueprintFields() {
        return incentiveBlueprintFields;
    }

    /**
     * List of fields under incentive blueprint
     * 
     */
    @JsonProperty("incentiveBlueprintFields")
    public void setIncentiveBlueprintFields(List<FieldDto> incentiveBlueprintFields) {
        this.incentiveBlueprintFields = incentiveBlueprintFields;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(incentiveBlueprintName).append(status).append(createdByUserId).append(createdDate).append(approvedByUserId).append(approvedDate).append(comments).append(incentiveBlueprintFields).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof IncentiveBlueprintDto) == false) {
            return false;
        }
        IncentiveBlueprintDto rhs = ((IncentiveBlueprintDto) other);
        return new EqualsBuilder().append(incentiveBlueprintName, rhs.incentiveBlueprintName).append(status, rhs.status).append(createdByUserId, rhs.createdByUserId).append(createdDate, rhs.createdDate).append(approvedByUserId, rhs.approvedByUserId).append(approvedDate, rhs.approvedDate).append(comments, rhs.comments).append(incentiveBlueprintFields, rhs.incentiveBlueprintFields).isEquals();
    }

}
