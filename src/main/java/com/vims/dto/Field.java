/**
 * 
 */
package com.vims.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * @author NGoyal
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "fieldName", "defaultFieldValue", "fieldDataType" })
public class Field {

	/**
	 * The name of the field
	 * 
	 */
	@JsonProperty("fieldName")
	@JsonPropertyDescription("The name of the field")
	private String fieldName;
	/**
	 * The default value set for the field
	 * 
	 */
	@JsonProperty("defaultFieldValue")
	@JsonPropertyDescription("The default value set for the field")
	private String defaultFieldValue;
	/**
	 * The data type set for the field
	 * 
	 */
	@JsonProperty("fieldDataType")
	@JsonPropertyDescription("The data type set for the field")
	private String fieldDataType;

	/**
	 * The name of the field
	 * 
	 */
	@JsonProperty("fieldName")
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * The name of the field
	 * 
	 */
	@JsonProperty("fieldName")
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * The default value set for the field
	 * 
	 */
	@JsonProperty("defaultFieldValue")
	public String getDefaultFieldValue() {
		return defaultFieldValue;
	}

	/**
	 * The default value set for the field
	 * 
	 */
	@JsonProperty("defaultFieldValue")
	public void setDefaultFieldValue(String defaultFieldValue) {
		this.defaultFieldValue = defaultFieldValue;
	}

	/**
	 * The data type set for the field
	 * 
	 */
	@JsonProperty("fieldDataType")
	public String getFieldDataType() {
		return fieldDataType;
	}

	/**
	 * The data type set for the field
	 * 
	 */
	@JsonProperty("fieldDataType")
	public void setFieldDataType(String fieldDataType) {
		this.fieldDataType = fieldDataType;
	}
}
