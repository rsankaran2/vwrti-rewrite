
package com.vims.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "modelCode",
    "modelYear",
    "modelName",
    "vehicleModelFields"
})
public class VehicleModelDto implements Serializable
{

    /**
     * The model code for vehicle
     * 
     */
    @JsonProperty("modelCode")
    @JsonPropertyDescription("The model code for vehicle")
    private String modelCode;
    /**
     * The model year for vehicle
     * 
     */
    @JsonProperty("modelYear")
    @JsonPropertyDescription("The model year for vehicle")
    private String modelYear;
    /**
     * The model name for vehicle
     * 
     */
    @JsonProperty("modelName")
    @JsonPropertyDescription("The model name for vehicle")
    private String modelName;
    /**
     * List of fields under vehicle
     * 
     */
    @JsonProperty("vehicleModelFields")
    @JsonPropertyDescription("List of fields under vehicle")
    @Valid
    private List<FieldDto> vehicleModelFields = new ArrayList<FieldDto>();
    private final static long serialVersionUID = -7490947747755746875L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public VehicleModelDto() {
    }

    /**
     * 
     * @param modelName
     * @param modelCode
     * @param modelYear
     * @param vehicleModelFields
     */
    public VehicleModelDto(String modelCode, String modelYear, String modelName, List<FieldDto> vehicleModelFields) {
        super();
        this.modelCode = modelCode;
        this.modelYear = modelYear;
        this.modelName = modelName;
        this.vehicleModelFields = vehicleModelFields;
    }

    /**
     * The model code for vehicle
     * 
     */
    @JsonProperty("modelCode")
    public String getModelCode() {
        return modelCode;
    }

    /**
     * The model code for vehicle
     * 
     */
    @JsonProperty("modelCode")
    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    /**
     * The model year for vehicle
     * 
     */
    @JsonProperty("modelYear")
    public String getModelYear() {
        return modelYear;
    }

    /**
     * The model year for vehicle
     * 
     */
    @JsonProperty("modelYear")
    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    /**
     * The model name for vehicle
     * 
     */
    @JsonProperty("modelName")
    public String getModelName() {
        return modelName;
    }

    /**
     * The model name for vehicle
     * 
     */
    @JsonProperty("modelName")
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    /**
     * List of fields under vehicle
     * 
     */
    @JsonProperty("vehicleModelFields")
    public List<FieldDto> getVehicleModelFields() {
        return vehicleModelFields;
    }

    /**
     * List of fields under vehicle
     * 
     */
    @JsonProperty("vehicleModelFields")
    public void setVehicleModelFields(List<FieldDto> vehicleModelFields) {
        this.vehicleModelFields = vehicleModelFields;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(modelCode).append(modelYear).append(modelName).append(vehicleModelFields).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof VehicleModelDto) == false) {
            return false;
        }
        VehicleModelDto rhs = ((VehicleModelDto) other);
        return new EqualsBuilder().append(modelCode, rhs.modelCode).append(modelYear, rhs.modelYear).append(modelName, rhs.modelName).append(vehicleModelFields, rhs.vehicleModelFields).isEquals();
    }

}
