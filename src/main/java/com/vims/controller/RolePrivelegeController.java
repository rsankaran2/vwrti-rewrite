package com.vims.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vims.constants.AppConstants;
import com.vims.request.PrivelegeRoleRequest;
import com.vims.response.GenericResponse;
import com.vims.response.RolePrivResponse;
import com.vims.service.RolePrivelegeService;

@RestController
@RequestMapping("/vims/role/privelege")
@Api(value = "RolePrivelegeController", description = "Operations include Role/Privileges Apis for authentication and authorization")
public class RolePrivelegeController {

	@Autowired
	RolePrivelegeService rolePrivelegeService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getAllPrivileges", method = RequestMethod.GET)
	@ApiOperation(value = "Gets all the privileges available in the system")
	public <T> GenericResponse<T> getAllPriveleges() {

		GenericResponse<T> resp = new GenericResponse<T>();

		List<RolePrivResponse> rolePrivDtoList = rolePrivelegeService
				.getAllPriveleges();

		if (!CollectionUtils.isEmpty(rolePrivDtoList)) {
			resp.setCode(HttpStatus.OK.value());
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);
			resp.setData((T) rolePrivDtoList);
		} else {
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getPrivilegesForRole/roleCode/{roleCode}", method = RequestMethod.GET)
	@ApiOperation(value = "Gets all the privileges associated with a specific Role")
	public <T> GenericResponse<T> getPrivilegesForRole(
			@PathVariable String roleCode) {

		GenericResponse<T> resp = new GenericResponse<T>();

		List<RolePrivResponse> rolePrivDtoList = rolePrivelegeService
				.getAllPrivelegesForRole(roleCode);

		if (!CollectionUtils.isEmpty(rolePrivDtoList)) {
			resp.setCode(HttpStatus.OK.value());
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);
			resp.setData((T) rolePrivDtoList);
		} else {
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}

	@RequestMapping(value = "/addPrivToRole", method = RequestMethod.POST)
	@ApiOperation(value = "Add Privileges to a role")
	public <T> GenericResponse<T> addPrivToRoles(
			@RequestBody PrivelegeRoleRequest privRoleRequest) {
		GenericResponse<T> resp = new GenericResponse<T>();
		
		Boolean isAdded = rolePrivelegeService.addPrivelegesToRole(privRoleRequest);
		
		if (BooleanUtils.isTrue(isAdded)) {
			resp.setCode(HttpStatus.OK.value());
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);
		} else {
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}

	@RequestMapping(value = "/updatePrivsToRole", method = RequestMethod.PUT)
	@ApiOperation(value = "Update Privileges to a role")
	public <T> GenericResponse<T> updatePrivToRoles(
			@RequestBody PrivelegeRoleRequest privRoleRequest) {
		GenericResponse<T> resp = new GenericResponse<T>();

		Boolean isAdded = true;
		if (BooleanUtils.isTrue(isAdded)) {
			resp.setCode(HttpStatus.OK.value());
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);
		} else {
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}

}
