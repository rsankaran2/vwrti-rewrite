package com.vims.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vims.constants.AppConstants;
import com.vims.dto.VehicleModelDto;
import com.vims.response.GenericResponse;
import com.vims.service.VehicleService;



@RestController
@RequestMapping("/vims/vehicle")
@Api(value="VehicleController", description="Vehicle related operations like addVehicle, updateVehicle,etc.")
public class VehicleController {
	
	@Autowired
	private VehicleService vehicleService;
	
	@RequestMapping(value = "/addVehicle", method = RequestMethod.POST)
	@ApiOperation(value="Add Vehicle")
	public <T> GenericResponse<T> addVehicle(@RequestBody VehicleModelDto vehicleModelDto) {
		 GenericResponse<T> resp = new GenericResponse<T>();
		//check category already exist or not, if yes then throw an error message otherwise add it
		
		/*if(false){
			 return new ResponseEntity(new GenericException("Category already exist"), HttpStatus.CONFLICT);
		}*/
		System.err.println("vehicleController:"+vehicleModelDto.getModelName()+vehicleModelDto.getModelYear()+vehicleModelDto.getModelCode());
		Boolean isAdded = vehicleService.addVehicle(vehicleModelDto);
		
		if(BooleanUtils.isTrue(isAdded)){
			resp.setCode(HttpStatus.OK.value());
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}
	
	@RequestMapping(value = "/updateVehicle", method = RequestMethod.PUT)
	@ApiOperation(value="Update Vehicle")
	public <T> GenericResponse<T> updateVehicle(@RequestBody VehicleModelDto vehicleModelDto) {
		 GenericResponse<T> resp = new GenericResponse<T>();
		//check category already exist or not, if yes then throw an error message otherwise add it
		
		/*if(false){
			 return new ResponseEntity(new GenericException("Category already exist"), HttpStatus.CONFLICT);
		}*/
		System.err.println("vehicleController:"+vehicleModelDto.getModelName()+vehicleModelDto.getModelYear()+vehicleModelDto.getModelCode());
		Boolean isAdded = vehicleService.addVehicle(vehicleModelDto);
		
		if(BooleanUtils.isTrue(isAdded)){
			resp.setCode(HttpStatus.OK.value());
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}
	
	
	@ApiOperation(value="Get vehicle information")
	@RequestMapping(value = "/getVehicle/modelName/{modelName}/modelCode/{modelCode}/modelYear/{modelYear}", method = RequestMethod.GET)
	public <T> GenericResponse<T> Vehicle(@PathVariable String modelName, @PathVariable String modelCode, @PathVariable String modelYear) {
		 GenericResponse<T> resp = new GenericResponse<T>();
		//check category already exist or not, if yes then throw an error message otherwise add it
		
		/*if(false){
			 return new ResponseEntity(new GenericException("Category already exist"), HttpStatus.CONFLICT);
		}*/
		
		Boolean isAdded = true;
		if(BooleanUtils.isTrue(isAdded)){
			resp.setCode(HttpStatus.OK.value());
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}
	
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String testRest() {
        		String test = "the spring rest api works!";
        		return test;
	}


	
	
}
