package com.vims.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.apache.commons.lang.BooleanUtils;
//import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vims.constants.AppConstants;
import com.vims.dto.Field;
import com.vims.dto.SubCategory;
import com.vims.dto.SubCategoryWrapper;
import com.vims.request.TopJson;
import com.vims.response.CategoryResponse;
import com.vims.response.GenericResponse;
import com.vims.response.SubCategoryResponse;
import com.vims.service.CategoryService;



@RestController
@RequestMapping("/vims")
@Api(value="CategoryController", description="Operations include to add,update,remove of category, subCategory, fields and also to get category,subCategory,fields information. ")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@RequestMapping(value = "/addCategories", method = RequestMethod.POST)
	@ApiOperation(value="Add Category")
	public <T> GenericResponse<T> addCategories(@RequestBody TopJson topJson) {
		GenericResponse<T> resp = new GenericResponse<T>();
		//check category already exist or not, if yes then throw an error message otherwise add it

		Boolean isAdded = categoryService.addCategories(topJson);

		if(BooleanUtils.isTrue(isAdded)){
			resp.setCode(HttpStatus.OK.value());
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}


	@RequestMapping(value = "/addSubCategory", method = RequestMethod.POST)
	@ApiOperation(value="Add SubCategory")
	public <T> GenericResponse<T> addSubCategory(@RequestBody TopJson topJson) {
		GenericResponse<T> resp = new GenericResponse<T>();
		//check category exist or not, if yes then continue otherwise throw an error message
		//check subCategory already exist or not, if exist then throw an error message otherwise add it

		Boolean isAdded = categoryService.addSubCategories(topJson);
		if(BooleanUtils.isTrue(isAdded)){
			resp.setCode(HttpStatus.OK.value()); 
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);

		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}

		return resp;
	}
	
	
	@SuppressWarnings("unchecked")
	@ApiOperation(value="Get Category Information by passing category name as input in request url")
	@RequestMapping(value = "/getCategory/{categoryName}", method = RequestMethod.GET)
	public <T> GenericResponse<T> getCategory(@PathVariable("categoryName") String categoryName) {
		 GenericResponse<T> resp = new GenericResponse<T>();
		
		CategoryResponse catResponse = categoryService.getCategory(categoryName);
		
		resp.setCode(HttpStatus.OK.value()); 
		resp.setMessage(AppConstants.SUCCESS_MESSAGE);
		resp.setData((T) catResponse);
		
		return resp;
	}
	
	


	@RequestMapping(value = "/addFields", method = RequestMethod.POST)
	@ApiOperation(value="Add Fields")
	public <T> GenericResponse<T> addFields(@RequestBody SubCategoryWrapper subCategoryWrapper) {
		GenericResponse<T> resp = new GenericResponse<T>();
		//check subCategory already exist or not, if no then throw an error message otherwise add it

		List<SubCategory> subCategoriesList = subCategoryWrapper.getSubCategoryList();
		
		if(CollectionUtils.isEmpty(subCategoriesList)){
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage("The subCategoriesList can not be null");	
		}
		
		Boolean isAdded = categoryService.addFields(subCategoriesList);
		
		if(BooleanUtils.isTrue(isAdded)){
			resp.setCode(HttpStatus.OK.value()); 
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);		
		}
		return resp;
	}


	@RequestMapping(value = "/updateFields", method = RequestMethod.PUT)
	@ApiOperation(value="Update Fields")
	public <T> GenericResponse<T> updateFields(@RequestBody  List<Field> typeFields) {
		GenericResponse<T> resp = new GenericResponse<T>();
		Boolean isAdded = false;
		if(!typeFields.isEmpty())
		{
			isAdded = categoryService.updateFields(typeFields);
		}
		else
		{
			//return error message if json is empty
		}

		//dao layer- return error message if field name is not available in database.

		if(BooleanUtils.isTrue(isAdded)){
			resp.setCode(HttpStatus.OK.value()); 
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);		
		}
		return resp;
	}

	@RequestMapping(value = "/deleteFields/{fieldnames}", method = RequestMethod.DELETE)
	@ApiOperation(value="Delete Fields")
	public <T> GenericResponse<T> deleteFields(@PathVariable String[] fieldnames) {
		GenericResponse<T> resp = new GenericResponse<T>();
		categoryService.deleteFields(fieldnames);
		resp.setCode(HttpStatus.OK.value()); 
		resp.setMessage(AppConstants.SUCCESS_MESSAGE);
		return resp;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value="Get Fields information")
	@RequestMapping(value = "/getFields/{subcategoryName}", method = RequestMethod.GET)
	public <T> GenericResponse<T> getFields(@PathVariable String subcategoryName) {
		GenericResponse<T> resp = new GenericResponse<T>();
		//check subCategory already exist or not, if no then throw an error message otherwise add it

		List<Field> fieldList = categoryService.getFields(subcategoryName);

		if(!CollectionUtils.isEmpty(fieldList)){
			resp.setCode(HttpStatus.OK.value()); 
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);
			resp.setData((T) fieldList);
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);		
		}
		return resp;
	}


	@RequestMapping(value = "/test", method = RequestMethod.GET)
	@ApiOperation(value="To test api is working or not")
	public <T> GenericResponse<T> testRest() {
		GenericResponse<T> resp = new GenericResponse<T>();
		//resp.setCode(HttpStatus.OK.value()); 
		resp.setCode(HttpStatus.OK.value());
		resp.setMessage("the spring rest api works!");
		return resp;
	}
	


	@RequestMapping(value = "/updateCategories", method = RequestMethod.POST)
	@ApiOperation(value="Update Category")
	public <T> GenericResponse<T> updateCategories(@RequestBody TopJson topJson) {
		GenericResponse<T> resp = new GenericResponse<T>();
		Boolean isAdded = categoryService.updateCategory(topJson);

		if(BooleanUtils.isTrue(isAdded)){
			resp.setCode(HttpStatus.OK.value()); 
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}

	
	@SuppressWarnings("unchecked")
	@ApiOperation(value="Get Sub Category Information by passing subCategory name as input in request url")
	@RequestMapping(value = "/getSubCategory/{subCategoryName}", method = RequestMethod.GET)
	public <T> GenericResponse<T> getSubCategory(@PathVariable("subCategoryName") String subCategoryName) {
		 GenericResponse<T> resp = new GenericResponse<T>();
		 
		 if(StringUtils.isEmpty(subCategoryName)){
			 resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			 resp.setMessage("subCategoryName can not be null or emply");
			 return resp;
		 }
		 
		 SubCategoryResponse subCatResponse = categoryService.getSubCategory(subCategoryName);	
		 
		 resp.setCode(HttpStatus.OK.value()); 
		 resp.setMessage(AppConstants.SUCCESS_MESSAGE);
		 resp.setData((T) subCatResponse);
		 
		 return resp;
		 
		 
	}

}
