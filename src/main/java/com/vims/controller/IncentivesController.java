package com.vims.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vims.constants.AppConstants;
import com.vims.dto.IncentiveBlueprintDto;
import com.vims.response.GenericResponse;
import com.vims.service.IncentiveService;



@RestController
@RequestMapping("/vims/incentives")
@Api(value="IncentivesController", description="Incentives related operations like addIncentives, getIncentives,etc.")
public class IncentivesController {
	
	@Autowired
	private IncentiveService incentiveService;
	
	@RequestMapping(value = "/addIncentive", method = RequestMethod.POST)
	@ApiOperation(value="Add Incentive")
	public <T> GenericResponse<T> addIncentive(@RequestBody IncentiveBlueprintDto incentiveBlueprintDto) {
		 GenericResponse<T> resp = new GenericResponse<T>();
		//check category already exist or not, if yes then throw an error message otherwise add it
		
		/*if(false){
			 return new ResponseEntity(new GenericException("Category already exist"), HttpStatus.CONFLICT);
		}*/
		Boolean isAdded = incentiveService.addInsentive(incentiveBlueprintDto);
		
		if(BooleanUtils.isTrue(isAdded)){

			resp.setCode(HttpStatus.OK.value());
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}
	
	@RequestMapping(value = "/updateIncentive", method = RequestMethod.PUT)
	@ApiOperation(value="Update Incentive")
	public <T> GenericResponse<T> updateIncentive(@RequestBody IncentiveBlueprintDto incentiveBlueprintDto) {
		 GenericResponse<T> resp = new GenericResponse<T>();
		//check category already exist or not, if yes then throw an error message otherwise add it
		
		/*if(false){
			 return new ResponseEntity(new GenericException("Category already exist"), HttpStatus.CONFLICT);
		}*/
		Boolean isAdded = incentiveService.addInsentive(incentiveBlueprintDto);
		
		if(BooleanUtils.isTrue(isAdded)){

			resp.setCode(HttpStatus.OK.value());
			resp.setMessage(AppConstants.SUCCESS_MESSAGE);
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}
	
	
	@ApiOperation(value="Get incentive information based for an incentiveBlueprintName")
	@RequestMapping(value = "/getIncentives/incentiveBluePrintName/{incentiveBlueprintName}", method = RequestMethod.GET)
	public <T> GenericResponse<T> getIncentives(@PathVariable String incentiveBlueprintName) {
		 GenericResponse<T> resp = new GenericResponse<T>();
		//check category already exist or not, if yes then throw an error message otherwise add it
		
		/*if(false){
			 return new ResponseEntity(new GenericException("Category already exist"), HttpStatus.CONFLICT);
		}*/
		
		Boolean isAdded = true;
		if(BooleanUtils.isTrue(isAdded)){
			resp.setCode(HttpStatus.OK.value());
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}
	
	@ApiOperation(value="Get all incentives")
	@RequestMapping(value = "/getAllIncentives", method = RequestMethod.GET)
	public <T> GenericResponse<T> getAllIncentives() {
		 GenericResponse<T> resp = new GenericResponse<T>();
		//check category already exist or not, if yes then throw an error message otherwise add it
		
		/*if(false){
			 return new ResponseEntity(new GenericException("Category already exist"), HttpStatus.CONFLICT);
		}*/
		
		Boolean isAdded = true;
		if(BooleanUtils.isTrue(isAdded)){
			resp.setCode(HttpStatus.OK.value());
		}else{
			resp.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			resp.setMessage(AppConstants.COMMON_ERROR_MSG);
		}
		return resp;
	}
}
