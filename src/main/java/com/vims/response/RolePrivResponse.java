package com.vims.response;

import java.io.Serializable;

public class RolePrivResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2742078205008219007L;
	
	private String privelegeCode;
	private String privelegeName;
	/**
	 * @return the privelegeCode
	 */
	public String getPrivelegeCode() {
		return privelegeCode;
	}
	/**
	 * @param privelegeCode the privelegeCode to set
	 */
	public void setPrivelegeCode(String privelegeCode) {
		this.privelegeCode = privelegeCode;
	}
	/**
	 * @return the privelegeName
	 */
	public String getPrivelegeName() {
		return privelegeName;
	}
	/**
	 * @param privelegeName the privelegeName to set
	 */
	public void setPrivelegeName(String privelegeName) {
		this.privelegeName = privelegeName;
	}
	
	
}
