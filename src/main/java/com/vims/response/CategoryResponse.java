/**
 * 
 */
package com.vims.response;

import java.util.List;

/**
 * @author NGoyal
 *
 */
public class CategoryResponse {

	private String categoryName;
	private List<SubCategoryResponse> subCategoryResponseList;
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}
	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	/**
	 * @return the subCategoryResponseList
	 */
	public List<SubCategoryResponse> getSubCategoryResponseList() {
		return subCategoryResponseList;
	}
	/**
	 * @param subCategoryResponseList the subCategoryResponseList to set
	 */
	public void setSubCategoryResponseList(
			List<SubCategoryResponse> subCategoryResponseList) {
		this.subCategoryResponseList = subCategoryResponseList;
	}

	
	
}
