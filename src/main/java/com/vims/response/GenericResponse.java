/**
 * 
 */
package com.vims.response;

/**
 * @author NGoyal
 *
 */
public class GenericResponse<T> {

	private Integer code;
	private String message;
	private T data;
	
	/**
	 * @param code
	 * @param message
	 * @param data
	 */
	
	public GenericResponse()
	{
		
	}

	public GenericResponse(int code, String message, T data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}
	
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public void setData(T data) {
		this.data = data;
	}
	
	
}
