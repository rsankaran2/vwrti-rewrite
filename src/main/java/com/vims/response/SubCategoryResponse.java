/**
 * 
 */
package com.vims.response;

import java.util.List;

/**
 * @author NGoyal
 *
 */
public class SubCategoryResponse {

	private String subCategoryName;
	private List<FieldResponse> fieldResponseList;
	/**
	 * @return the subCategoryName
	 */
	public String getSubCategoryName() {
		return subCategoryName;
	}
	/**
	 * @param subCategoryName the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	/**
	 * @return the fieldResponseList
	 */
	public List<FieldResponse> getFieldResponseList() {
		return fieldResponseList;
	}
	/**
	 * @param fieldResponseList the fieldResponseList to set
	 */
	public void setFieldResponseList(List<FieldResponse> fieldResponseList) {
		this.fieldResponseList = fieldResponseList;
	}
	
	
}
