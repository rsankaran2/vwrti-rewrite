/**
 * 
 */
package com.vims.response;

/**
 * @author NGoyal
 *
 */
public class FieldResponse {

	private String fieldName;
	private String fieldDefaultValue;
	private String fieldDataType;
	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}
	/**
	 * @param fieldName the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	/**
	 * @return the fieldDefaultValue
	 */
	public String getFieldDefaultValue() {
		return fieldDefaultValue;
	}
	/**
	 * @param fieldDefaultValue the fieldDefaultValue to set
	 */
	public void setFieldDefaultValue(String fieldDefaultValue) {
		this.fieldDefaultValue = fieldDefaultValue;
	}
	/**
	 * @return the fieldDataType
	 */
	public String getFieldDataType() {
		return fieldDataType;
	}
	/**
	 * @param fieldDataType the fieldDataType to set
	 */
	public void setFieldDataType(String fieldDataType) {
		this.fieldDataType = fieldDataType;
	}
	
	
}
