/**
 * 
 */
package com.vims.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vims.dao.CategoryDao;
import com.vims.dao.IncentiveDao;
import com.vims.dto.FieldDto;
import com.vims.dto.IncentiveBlueprintDto;
import com.vims.model.IncentiveBlueprint;
import com.vims.model.IncentiveBlueprintFields;
import com.vims.model.IncentiveBlueprintFieldsPK;
import com.vims.model.TypeField;
import com.vims.service.IncentiveService;

@Service
public class IncentiveServiceImpl implements IncentiveService {

	@Autowired
	private CategoryDao categoryDao;
	
	@Autowired
	private IncentiveDao incentiveDao;

	
	/* (non-Javadoc)
	 * @see com.volkswagen.service.VehicleService#addVehicle(com.volkswagen.dto.VehicleModelDto)
	 */
	@Override
	public Boolean addInsentive(IncentiveBlueprintDto incentiveBlueprintDto) {
		IncentiveBlueprint model = new IncentiveBlueprint();
		IncentiveBlueprintFields modelFields = null;
		List<IncentiveBlueprintFields> modelFieldsList = new ArrayList<IncentiveBlueprintFields>();
		IncentiveBlueprintFieldsPK modelPk = null;
		
		List<FieldDto> incentiveFieldsList = incentiveBlueprintDto.getIncentiveBlueprintFields();
		System.err.println("bpName:"+incentiveBlueprintDto.getIncentiveBlueprintName());
		
		model.setApprovedByUserId(incentiveBlueprintDto.getApprovedByUserId());
		model.setApprovedDate(incentiveBlueprintDto.getApprovedDate());
		model.setComments(incentiveBlueprintDto.getComments());
		model.setCreatedByUserId(incentiveBlueprintDto.getCreatedByUserId());
		model.setCreatedDate(incentiveBlueprintDto.getCreatedDate());
		model.setIncentiveBlueprintName(incentiveBlueprintDto.getIncentiveBlueprintName());
		model.setStatus(incentiveBlueprintDto.getStatus());
		
		
		if(!incentiveFieldsList.isEmpty())
		{
			for (FieldDto field : incentiveFieldsList) {
				modelFields = new IncentiveBlueprintFields();
				modelPk = new IncentiveBlueprintFieldsPK();
				
				System.err.println("fieldname:"+field.getFieldName());
				System.err.println("fieldvalue:"+field.getFieldValue());
				TypeField typeField = categoryDao.findTypeFieldByName(field.getFieldName());
				System.err.println("typeFieldId:"+typeField.getTypeFieldId());
				
				modelPk.setTypeField(typeField);
				modelPk.setIncentiveBlueprint(model);
				
				System.err.println("modelId:"+ model.getIncentiveBlueprintId());
				
				modelFields.setTypeFieldValue(field.getFieldValue());
				modelFields.setIncentiveBlueprintFieldsPK(modelPk);
				
				modelFieldsList.add(modelFields); 
				
			}
		}

		model.setModelFields(modelFieldsList);
		boolean result = incentiveDao.addIncentive(model);
		return result;
		
	}

}
