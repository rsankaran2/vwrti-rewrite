/**
 * 
 */
package com.vims.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vims.dao.CategoryDao;
import com.vims.dto.Category;
import com.vims.dto.Field;
import com.vims.dto.SubCategory;
import com.vims.model.TypeCategory;
import com.vims.model.TypeField;
import com.vims.model.TypeSubCategory;
import com.vims.request.TopJson;
import com.vims.response.CategoryResponse;
import com.vims.response.FieldResponse;
import com.vims.response.SubCategoryResponse;
import com.vims.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryDao categoryDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.volkswagen.service.CategoryService#addCategories(com.volkswagen.dto
	 * .TopJson)
	 */
	@Override
	public Boolean addCategories(TopJson topJson) {

		List<TypeCategory> typeCategories = new ArrayList<>();

		List<Category> categories = topJson.getCategoryList();

		for (Category category : categories) {
			TypeCategory typeCategory = new TypeCategory();

			typeCategory.setTypeCategoryName(category.getCategoryName());
			List<SubCategory> subCategories = category.getSubCategoryList();

			if (!CollectionUtils.isEmpty(subCategories)) {

				List<TypeSubCategory> typeSubCategories = new ArrayList<>();
				for (SubCategory subCategory : subCategories) {
					TypeSubCategory typeSubCategory = new TypeSubCategory();

					typeSubCategory.setTypeSubCategoryName(subCategory
							.getSubCategoryName());

					List<Field> fields = subCategory.getTypeFields();

					if (!CollectionUtils.isEmpty(fields)) {
						List<TypeField> typeFields = new ArrayList<>();

						for (Field field : fields) {
							TypeField typeField = new TypeField();

							typeField.setTypeFieldDataType(field
									.getFieldDataType());
							typeField.setTypeFieldDefaultValue(field
									.getDefaultFieldValue());
							typeField.setTypeFieldName(field.getFieldName());

							typeField.setSubCategory(typeSubCategory);
							typeFields.add(typeField);
						}

						typeSubCategory.setTypeFields(typeFields);
					}

					typeSubCategory.setCategory(typeCategory);
					typeSubCategories.add(typeSubCategory);
				}

				typeCategory.setSubCategories(typeSubCategories);
			}

			typeCategories.add(typeCategory);
		}

		return categoryDao.addCategories(typeCategories);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.volkswagen.service.CategoryService#addSubCategories(com.volkswagen
	 * .dto.TopJson)
	 */
	@Override
	public Boolean addSubCategories(TopJson topJson) {

		List<TypeSubCategory> typeSubCategories = new ArrayList<>();

		List<Category> categories = topJson.getCategoryList();

		if (CollectionUtils.isEmpty(categories)) {

			for (Category category : categories) {

				TypeCategory typeCatg = categoryDao.findCategoryByName(category
						.getCategoryName());

				if (typeCatg != null) {

					List<SubCategory> subCategories = category
							.getSubCategoryList();

					if (!CollectionUtils.isEmpty(subCategories)) {

						for (SubCategory subCategory : subCategories) {

							TypeSubCategory typeSubCategory = new TypeSubCategory();

							typeSubCategory.setTypeSubCategoryName(subCategory
									.getSubCategoryName());

							typeSubCategory.setCategory(typeCatg);

							typeSubCategories.add(typeSubCategory);

						}

					} else {
						return false;
					}

				} else {
					return false;
				}
			}

			return categoryDao.addSubCategories(typeSubCategories);
		} else {
			return false;
		}

	}

	/*
	 * (non-Javadoc)
	 * @see com.volkswagen.service.CategoryService#addFields(java.util.List)
	 */
	@Override
	public Boolean addFields(List<SubCategory> subCategoriesList) {

		for (SubCategory subCategory : subCategoriesList) {

			TypeSubCategory typeSubCategory = categoryDao
					.findSubCategoryByName(subCategory.getSubCategoryName());

			if (typeSubCategory != null) {

				List<TypeField> typeFieldsList = new ArrayList<>();
				List<Field> fieldsList = subCategory.getTypeFields();

				for (Field field : fieldsList) {

					TypeField typeField = new TypeField();

					typeField.setSubCategory(typeSubCategory);
					typeField.setTypeFieldDataType(field.getFieldDataType());
					typeField.setTypeFieldDefaultValue(field
							.getDefaultFieldValue());
					typeField.setTypeFieldName(field.getFieldName());

					typeFieldsList.add(typeField);

				}

				categoryDao.addFields(typeFieldsList);
			}
		}

		return true;
	}

	@Override
	public Boolean updateCategory(TopJson json) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Boolean updateSubCategory(TopJson json) {
		// TODO Auto-generated method stub
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see com.volkswagen.service.CategoryService#updateFields(java.util.List)
	 */
	@Override
	public Boolean updateFields(List<Field> typeFieldsDto) {
		boolean status = false;
		List<TypeField> typeFields = null;
		if (!CollectionUtils.isEmpty(typeFieldsDto)) {
			typeFields = new ArrayList<>();

			for (Field field : typeFieldsDto) {

				TypeField existingField = categoryDao.findTypeFieldByName(field
						.getFieldName());

				if (StringUtils.isNotEmpty(field.getFieldDataType())) {
					existingField
							.setTypeFieldDataType(field.getFieldDataType());
				}

				if (StringUtils.isNotEmpty(field.getDefaultFieldValue())) {
					existingField.setTypeFieldDefaultValue(field
							.getDefaultFieldValue());
				}

				typeFields.add(existingField);
			}

		}
		List<TypeField> fieldsUpdated = categoryDao.updateFields(typeFields);
		if (!CollectionUtils.isEmpty(fieldsUpdated)) {
			status = true;
		}
		return status;
	}

	/*
	 * (non-Javadoc)
	 * @see com.volkswagen.service.CategoryService#deleteFields(java.lang.String[])
	 */
	@Override
	public void deleteFields(String[] fieldNames) {
		List<TypeField> typeFieldList = new ArrayList<TypeField>();
		if (fieldNames != null && fieldNames.length > 0) {
			for (String s : fieldNames) {

				if (!StringUtils.isBlank(s)) {
					TypeField field = categoryDao.findTypeFieldByName(s); // can
																			// throw
																			// error
					typeFieldList.add(field);
				}
				// if blank do nothing, skip to next fieldName
			}

			categoryDao.deleteFields(typeFieldList);

		} else {
			// error string
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.volkswagen.service.CategoryService#getFields(java.lang.String)
	 */
	@Override
	public List<Field> getFields(String subCategory) {
		TypeSubCategory subcategory = categoryDao
				.findSubCategoryByName(subCategory);
		List<Field> fieldList = new ArrayList<Field>();
		Field fieldDto = null;
		// if subcategory cannot be foud throw error

		System.err.println("subcatid:" + subcategory.getTypeSubCategoryId());

		List<TypeField> typefieldModelList = categoryDao
				.findTypeFieldById(subcategory.getTypeSubCategoryId());

		if (!CollectionUtils.isEmpty(typefieldModelList)) {
			for (TypeField modelField : typefieldModelList) {
				fieldDto = new Field();
				fieldDto.setFieldName(modelField.getTypeFieldName());
				fieldDto.setFieldDataType(modelField.getTypeFieldDataType());
				fieldDto.setDefaultFieldValue(modelField
						.getTypeFieldDefaultValue());
				fieldList.add(fieldDto);
			}

		}
		return fieldList;
	}

	/*
	 * (non-Javadoc)
	 * @see com.volkswagen.service.CategoryService#getCategory(java.lang.String)
	 */
	@Override
	public CategoryResponse getCategory(String categoryName) {

		CategoryResponse categoryResponse = null;
		TypeCategory typeCategory = categoryDao.findCategoryByName(categoryName);

		if(typeCategory == null){
			return categoryResponse;
		}
		
			 categoryResponse = new CategoryResponse();
			
			categoryResponse
					.setCategoryName(typeCategory.getTypeCategoryName());

			List<TypeSubCategory> typeSubCategories = typeCategory
					.getSubCategories();

			if (!CollectionUtils.isEmpty(typeSubCategories)) {

				List<SubCategoryResponse> subCategoryResponses = new ArrayList<>();

				for (TypeSubCategory typeSubCategory : typeSubCategories) {

					SubCategoryResponse subCategoryResponse = new SubCategoryResponse();

					subCategoryResponse.setSubCategoryName(typeSubCategory
							.getTypeSubCategoryName());

					List<TypeField> typeFields = typeSubCategory
							.getTypeFields();

					if (!CollectionUtils.isEmpty(typeFields)) {

						List<FieldResponse> fieldResponses = new ArrayList<>();

						for (TypeField typeField : typeFields) {

							FieldResponse fieldResponse = new FieldResponse();

							fieldResponse.setFieldDataType(typeField
									.getTypeFieldDataType());
							fieldResponse.setFieldDefaultValue(typeField
									.getTypeFieldDefaultValue());
							fieldResponse.setFieldName(typeField
									.getTypeFieldName());

							fieldResponses.add(fieldResponse);

						}

						subCategoryResponse
								.setFieldResponseList(fieldResponses);
					}

					subCategoryResponses.add(subCategoryResponse);
				}

				categoryResponse
						.setSubCategoryResponseList(subCategoryResponses);
			}

		return categoryResponse;
	}

	/*
	 * (non-Javadoc)
	 * @see com.volkswagen.service.CategoryService#getSubCategory(java.lang.String)
	 */
	@Override
	public SubCategoryResponse getSubCategory(String subCategoryName) {
		
		SubCategoryResponse subCategoryResponse = null;
		TypeSubCategory typeSubCategory = categoryDao.findSubCategoryByName(subCategoryName);
		
		if(typeSubCategory== null){
			return subCategoryResponse;
		}
		
		subCategoryResponse = new SubCategoryResponse();
		
		subCategoryResponse.setSubCategoryName(typeSubCategory.getTypeSubCategoryName());
		
		List<TypeField> typeFields = typeSubCategory.getTypeFields();
		
		if(!CollectionUtils.isEmpty(typeFields)){
			
			List<FieldResponse> fieldRespList = new ArrayList<>();
			
			for (TypeField typeField : typeFields) {
				FieldResponse fieldResponse = new FieldResponse();
				
				fieldResponse.setFieldDataType(typeField.getTypeFieldDataType());
				fieldResponse.setFieldDefaultValue(typeField.getTypeFieldDefaultValue());
				fieldResponse.setFieldName(typeField.getTypeFieldName());
				
				fieldRespList.add(fieldResponse);
			}
			
			subCategoryResponse.setFieldResponseList(fieldRespList);
			
		}
		
		
		return subCategoryResponse;
	}

}
