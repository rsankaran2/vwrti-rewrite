/**
 * 
 */
package com.vims.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vims.dao.CategoryDao;
import com.vims.dao.VehicleDao;
import com.vims.dto.FieldDto;
import com.vims.dto.VehicleModelDto;
import com.vims.model.TypeField;
import com.vims.model.VehicleModel;
import com.vims.model.VehicleModelFields;
import com.vims.model.VehicleModelFieldsPK;
import com.vims.service.VehicleService;

@Service
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private CategoryDao categoryDao;
	
	@Autowired
	private VehicleDao vehicleDao;

	
	/* (non-Javadoc)
	 * @see com.volkswagen.service.VehicleService#addVehicle(com.volkswagen.dto.VehicleModelDto)
	 */
	@Override
	public Boolean addVehicle(VehicleModelDto vehicleModelDto) {
		VehicleModel model = new VehicleModel();
		VehicleModelFields modelFields = null;
		List<VehicleModelFields> modelFieldsList = new ArrayList<VehicleModelFields>();
		VehicleModelFieldsPK modelPk = null;
		
		List<FieldDto> vehicleFieldsList = vehicleModelDto.getVehicleModelFields();
		System.err.println("vehiclemodelcode:"+vehicleModelDto.getModelCode());
		model.setVehicleModelCode(vehicleModelDto.getModelCode());
		model.setVehicleModelName(vehicleModelDto.getModelName());
		model.setVehicleModelYear(vehicleModelDto.getModelYear());
		
		
		if(!vehicleFieldsList.isEmpty())
		{
			for (FieldDto field : vehicleFieldsList) {
				modelFields = new VehicleModelFields();
				modelPk = new VehicleModelFieldsPK();
				
				System.err.println("fieldname:"+field.getFieldName());
				System.err.println("fieldvalue:"+field.getFieldValue());
				TypeField typeField = categoryDao.findTypeFieldByName(field.getFieldName());
				System.err.println("typeFieldId:"+typeField.getTypeFieldId());
				
				modelPk.setTypeField(typeField);
				modelPk.setVehicleModel(model);
				
				System.err.println("modelId:"+ model.getVehicleModelId());
				
				modelFields.setTypeFieldValue(field.getFieldValue());
				modelFields.setVehicleModelFieldsPK(modelPk);
				
				modelFieldsList.add(modelFields); 
				
			}
		}

		model.setModelFields(modelFieldsList);
		boolean result = vehicleDao.addVehicle(model);
		return result;
		
	}

}
