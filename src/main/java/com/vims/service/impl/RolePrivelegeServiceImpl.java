/**
 * 
 */
package com.vims.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.vims.dao.RolePrivelegeDao;
import com.vims.model.Priveleges;
import com.vims.model.Roles;
import com.vims.request.PrivelegeRoleRequest;
import com.vims.request.PrivelegesRequest;
import com.vims.response.RolePrivResponse;
import com.vims.service.RolePrivelegeService;

/**
 * @author NGoyal
 *
 */
@Service
@Transactional(readOnly=true)
public class RolePrivelegeServiceImpl implements RolePrivelegeService {

	
	@Autowired
	RolePrivelegeDao rolePrivelegeDao;
	
	/* (non-Javadoc)
	 * @see com.vims.service.RolePrivelegeService#getAllPriveleges()
	 */
	@Override
	@Transactional
	public List<RolePrivResponse> getAllPriveleges() {
		
		List<Priveleges> privelegesList = rolePrivelegeDao.getAllPriveleges();
		
		if(!CollectionUtils.isEmpty(privelegesList)){
			
			List<RolePrivResponse> rolePrivDtoList = new ArrayList<>();
			
			for (Priveleges priveleges : privelegesList) {
				
				RolePrivResponse rolePrivDto = new RolePrivResponse();
				
				rolePrivDto.setPrivelegeCode(priveleges.getPrivCode());
				rolePrivDto.setPrivelegeName(priveleges.getPrivName());
				
				rolePrivDtoList.add(rolePrivDto);
			}
			
			return rolePrivDtoList;
		}
		
		return null;
	}

	@Override
	@Transactional
	public List<RolePrivResponse> getAllPrivelegesForRole(String roleCode) {
		
		Roles roles = rolePrivelegeDao.getAllPrivelegesForRole(roleCode);
		
		if(roles == null){
			return null;
		}
		
		Set<Priveleges> privelegesList = roles.getPriveleges();
		
		if(!CollectionUtils.isEmpty(privelegesList)){
			
			List<RolePrivResponse> rolePrivDtoList = new ArrayList<>();
			
			for (Priveleges priveleges : privelegesList) {
				
				RolePrivResponse rolePrivDto = new RolePrivResponse();
				
				rolePrivDto.setPrivelegeCode(priveleges.getPrivCode());
				rolePrivDto.setPrivelegeName(priveleges.getPrivName());
				
				rolePrivDtoList.add(rolePrivDto);
			}
			
			return rolePrivDtoList;
		}
		
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public Boolean addPrivelegesToRole(PrivelegeRoleRequest privelegeRoleRequest) {
		
		Roles role = rolePrivelegeDao.findRoleByCode(privelegeRoleRequest.getRoleCode());
		
		if(role == null){
			return false;
		}
		
		List<PrivelegesRequest> privelegeRequestList =  privelegeRoleRequest.getPriveleges();
		
		//Set<Priveleges> privelegesList = new HashSet<>();
		
		Set<Priveleges> privelegesSet = role.getPriveleges();
		
		for (PrivelegesRequest privelegesRequest : privelegeRequestList) {
			
			Priveleges priveleges = new Priveleges();
			priveleges.setPrivCode(privelegesRequest.getPrivCode());
			priveleges.setPrivName(privelegesRequest.getPrivName());
			privelegesSet.add(priveleges);
		}
		
		role.setPriveleges(privelegesSet);
		
		return rolePrivelegeDao.addPrivelegesToRole(role);
	}

	@Override
	public Boolean updatePrivsToRole(PrivelegeRoleRequest privelegeRoleRequest) {
		return null;
	}

}
