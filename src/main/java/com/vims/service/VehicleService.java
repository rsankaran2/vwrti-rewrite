package com.vims.service;

import com.vims.dto.VehicleModelDto;


public interface VehicleService {

	public Boolean addVehicle(VehicleModelDto vehicleModelDto);
	
}
