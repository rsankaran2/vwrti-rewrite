package com.vims.service;

import java.util.List;

import com.vims.dto.Field;
import com.vims.dto.SubCategory;
import com.vims.request.TopJson;
import com.vims.response.CategoryResponse;
import com.vims.response.SubCategoryResponse;


public interface CategoryService {

	public Boolean addCategories(TopJson topJson);
	
	public Boolean addSubCategories(TopJson topJson);
	
	public Boolean addFields(List<SubCategory> subCategoriesList);
	
	public Boolean updateCategory(TopJson json);
	
	public Boolean updateSubCategory(TopJson json);
	
	public Boolean updateFields(List<Field> typeFields);
	
	public List<Field> getFields(String subCategory);
	
	public void deleteFields(String[] fieldNames);
	
	public CategoryResponse getCategory(String categoryName);
	
	public SubCategoryResponse getSubCategory(String subCategoryName);
	
	
}
