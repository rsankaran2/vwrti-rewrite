/**
 * 
 */
package com.vims.service;

import java.util.List;

import com.vims.request.PrivelegeRoleRequest;
import com.vims.response.RolePrivResponse;

/**
 * @author NGoyal
 *
 */
public interface RolePrivelegeService {

	public List<RolePrivResponse> getAllPriveleges();
	
	public List<RolePrivResponse> getAllPrivelegesForRole(String roleCode);
	
	public Boolean addPrivelegesToRole(PrivelegeRoleRequest privelegeRoleRequest);
	
	public Boolean updatePrivsToRole(PrivelegeRoleRequest privelegeRoleRequest);
}
