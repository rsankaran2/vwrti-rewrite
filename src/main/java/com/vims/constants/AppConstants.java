package com.vims.constants;

public interface AppConstants {
	
	 public static final String COMMON_ERROR_MSG = "Error in processing the request!";
	 public static final String SUCCESS_MESSAGE = "Success";
	 public static final String SUCCESS_CODE = "000";
	 public static final String ERROR_CODE = "101";

}
