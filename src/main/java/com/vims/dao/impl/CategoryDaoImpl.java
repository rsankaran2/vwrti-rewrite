package com.vims.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vims.dao.CategoryDao;
import com.vims.model.TypeCategory;
import com.vims.model.TypeField;
import com.vims.model.TypeSubCategory;
import com.vims.repository.TypeCategoryRepository;
import com.vims.repository.TypeFieldRepository;
import com.vims.repository.TypeSubCategoryRepository;



@Repository
public class CategoryDaoImpl implements CategoryDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private TypeCategoryRepository typeCategoryRepository;
	
	@Autowired
	private TypeFieldRepository typeFieldRepository;
	
	@Autowired
	private TypeSubCategoryRepository typeSubCategoryRepository;
	
	@Override
	public Boolean addCategories(List<TypeCategory> typeCategories) {
		
		typeCategoryRepository.save(typeCategories);
		return true;
	}

	@Override
	public Boolean addSubCategories(List<TypeSubCategory> typeSubCategories) {
		typeSubCategoryRepository.save(typeSubCategories);
		return true;
	}

	@Override
	public Boolean addFields(List<TypeField> typeFields) {
		 typeFieldRepository.save(typeFields);
		 return true;
	}

	@Override
	public TypeCategory findCategoryByName(String name) {
		return typeCategoryRepository.findByName(name);
	}
	
	
	@Override
	public TypeSubCategory findSubCategoryByName(String name) {
		return typeSubCategoryRepository.findByName(name);
	}

	@Override
	public TypeField findTypeFieldByName(String name) {
		return typeFieldRepository.findTypeFieldByName(name);
	}

	@Override
	public List<TypeField> updateFields(List<TypeField> typeFields) {
		return typeFieldRepository.save(typeFields);
	}

	@Override
	public void deleteFields(List<TypeField> typeFieldList) {
		typeFieldRepository.delete(typeFieldList);
	}

	@Override
	public List<TypeField> findTypeFieldById(Integer id) {
		
		return typeFieldRepository.findTypeFieldById(id);
	}
	
}
