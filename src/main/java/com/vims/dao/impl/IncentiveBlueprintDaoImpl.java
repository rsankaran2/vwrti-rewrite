package com.vims.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vims.dao.IncentiveDao;
import com.vims.model.IncentiveBlueprint;
import com.vims.repository.IncentivesRepository;



@Repository
public class IncentiveBlueprintDaoImpl implements IncentiveDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private IncentivesRepository incentivesRepository;
	


	@Override
	public Boolean addIncentive(IncentiveBlueprint incentiveBlueprint) {
		IncentiveBlueprint model = null;
		System.err.print("IncentiveBlueprintDaoImpl:name:"+incentiveBlueprint.getIncentiveBlueprintName());
		model = incentivesRepository.save(incentiveBlueprint);
		if(null!=model)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	

}
