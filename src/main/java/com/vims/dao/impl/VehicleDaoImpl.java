package com.vims.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vims.dao.VehicleDao;
import com.vims.model.VehicleModel;
import com.vims.repository.VehicleRepository;



@Repository
public class VehicleDaoImpl implements VehicleDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private VehicleRepository vehicleRepository;
	


	@Override
	public Boolean addVehicle(VehicleModel vehicleModel) {
		VehicleModel model = null;
		System.err.print("vehicleDaoImpl:vehicleModelCode:"+vehicleModel.getVehicleModelCode());
		model = vehicleRepository.save(vehicleModel);
		if(null!=model)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	

}
