/**
 * 
 */
package com.vims.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vims.dao.RolePrivelegeDao;
import com.vims.model.Priveleges;
import com.vims.model.Roles;
import com.vims.repository.PrivelegeRepository;
import com.vims.repository.RolesRepository;

/**
 * @author NGoyal
 *
 */
@Repository
public class RolePrivelegeDaoImpl implements RolePrivelegeDao {

	@Autowired
	PrivelegeRepository privelegeRepository;
	
	@Autowired
	RolesRepository roleRepository;
	
	
	/* (non-Javadoc)
	 * @see com.vims.dao.RolePrivelegeDao#getAllPriveleges()
	 */
	@Override
	public List<Priveleges> getAllPriveleges() {
		return privelegeRepository.findAll();
	}


	@Override
	public Roles getAllPrivelegesForRole(String roleCode) {
		return roleRepository.findRoleByCode(roleCode);
	}


	@Override
	public Roles findRoleByCode(String roleCode) {
		return roleRepository.findRoleByCode(roleCode);
	}


	@Override
	public Boolean addPrivelegesToRole(Roles role) {
		
		roleRepository.save(role);
		return true;
	}

}
