package com.vims.dao;

import com.vims.model.IncentiveBlueprint;



public interface IncentiveDao {

	public Boolean addIncentive(IncentiveBlueprint incentiveBlueprint);
	
}	
