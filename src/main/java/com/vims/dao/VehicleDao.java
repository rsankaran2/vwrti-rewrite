package com.vims.dao;

import com.vims.model.VehicleModel;



public interface VehicleDao {

	public Boolean addVehicle(VehicleModel vehicleModel);
	
}	
