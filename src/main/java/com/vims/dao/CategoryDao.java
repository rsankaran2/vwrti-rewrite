package com.vims.dao;

import java.util.List;

import com.vims.model.TypeCategory;
import com.vims.model.TypeField;
import com.vims.model.TypeSubCategory;



public interface CategoryDao {

	public Boolean addCategories(List<TypeCategory> typeCategories);

	public Boolean addSubCategories(List<TypeSubCategory> typeSubCategories);

	public Boolean addFields(List<TypeField> typeFields);

	public TypeCategory findCategoryByName(String name);
	
	public TypeSubCategory findSubCategoryByName(String name);

	public TypeField findTypeFieldByName(String name);
	
    public List<TypeField> findTypeFieldById(Integer id);
	
	public List<TypeField> updateFields(List<TypeField> typeFields);
	
	public void deleteFields(List<TypeField> typeFieldList);
	
}
