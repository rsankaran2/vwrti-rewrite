/**
 * 
 */
package com.vims.dao;

import java.util.List;

import com.vims.model.Priveleges;
import com.vims.model.Roles;

/**
 * @author NGoyal
 *
 */
public interface RolePrivelegeDao {

	public List<Priveleges> getAllPriveleges();

	public Roles getAllPrivelegesForRole(String roleCode);
	
	public Roles findRoleByCode(String roleCode);
	
	public Boolean addPrivelegesToRole(Roles role);
}
