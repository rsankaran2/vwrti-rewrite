/**
 * 
 */
package com.vims.exception;

/**
 * @author NGoyal
 *
 */
public class GenericException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public GenericException(String errorMessage){
		this.errorMessage = errorMessage;
	}
	
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	
}
