/**
 * 
 */
package com.vims.request;

/**
 * @author NGoyal
 *
 */
public class PrivelegesRequest {

	private String privCode;
	private String privName;
	/**
	 * @return the privCode
	 */
	public String getPrivCode() {
		return privCode;
	}
	/**
	 * @param privCode the privCode to set
	 */
	public void setPrivCode(String privCode) {
		this.privCode = privCode;
	}
	/**
	 * @return the privName
	 */
	public String getPrivName() {
		return privName;
	}
	/**
	 * @param privName the privName to set
	 */
	public void setPrivName(String privName) {
		this.privName = privName;
	}
	
	
}
