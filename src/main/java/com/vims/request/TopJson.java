/**
 * 
 */
package com.vims.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.vims.dto.Category;

/**
 * @author NGoyal
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"categoryList"
})
public class TopJson {

	
	/**
	* One or more categories with subcategories
	* 
	*/
	@JsonProperty("categoryList")
	@JsonPropertyDescription("One or more categories with subcategories")
	private List<Category> categoryList = null;

	/**
	* One or more categories with subcategories
	* 
	*/
	@JsonProperty("categoryList")
	public List<Category> getCategoryList() {
	return categoryList;
	}

	/**
	* One or more categories with subcategories
	* 
	*/
	@JsonProperty("categoryList")
	public void setCategoryList(List<Category> categoryList) {
	this.categoryList = categoryList;
	}


}
