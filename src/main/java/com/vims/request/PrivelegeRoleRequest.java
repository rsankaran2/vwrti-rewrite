/**
 * 
 */
package com.vims.request;

import java.util.List;

/**
 * @author NGoyal
 *
 */
public class PrivelegeRoleRequest {

	private String roleCode;
	private List<PrivelegesRequest> priveleges;
	/**
	 * @return the roleCode
	 */
	public String getRoleCode() {
		return roleCode;
	}
	/**
	 * @param roleCode the roleCode to set
	 */
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	/**
	 * @return the priveleges
	 */
	public List<PrivelegesRequest> getPriveleges() {
		return priveleges;
	}
	/**
	 * @param priveleges the priveleges to set
	 */
	public void setPriveleges(List<PrivelegesRequest> priveleges) {
		this.priveleges = priveleges;
	}
	
	
	
}
